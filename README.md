# Pool Time

Pool Time - a small library for defining probabilistic events in javascript. In development.

## Why?
Timed events in JavaScript effectively work in three ways: you either use
`setTimeout` / `setInterval`, count the difference between successive calls to
`performance.now`, or wait for an Event, Promise, or Observable. Each of these
has drawbacks: the first two APIs provide linear access to time, and waiting
for Events is unpredictable and generally requires input. How do we account
for use cases which call for more dynamically-defined timing?

Enter Pool Time. Pool Time is an Obversable-based wrapper around linear timing
APIs (currently `setTimeout`) which enables developers to simply express dynamic
timing . The library relies on defining a period of time -- a `Pool` -- in which
a number of things (drops) happen. A `Pool` has a duration, whereas a drop is
simply an Observable callback and has no duration. All of the parameters can be
expressed dynamically (as an arrow function) or statically, and special classes
are provided for playing back video (`VideoPool`) and audio (`AudioPool`, which 
uses Howler.js under-the-hood).

## Usage
*Not yet available through regular distrobution channels; sorry! Coming soon to NPM. Feel free to use as a `git` submodule.*
```typescript
import { Pool, AudioPool, VideoPool } from './pool-time';
```

## Documentation
[pool-time.gitlab.io](https://pool-time.gitlab.io)

## Examples

### Log the time five times in one minute
```typescript
const pool = new Pool({
    duration: 60 * 1000,
    dropCount: 5
});

pool.drop$.subscribe(() => console.log(new Date()));
pool.start();
```
```
0"                                              60"
|                                                |
|           *            *     *    *   *        |
|________________________________________________|
```
```
2019-05-07T15:41:15.741Z
2019-05-07T15:41:28.865Z
2019-05-07T15:41:31.212Z
2019-05-07T15:41:35.826Z
2019-05-07T15:41:40.317Z
```

### Log the time 1-3 times every 60-90 seconds, grouping them
```typescript
const pool = new Pool({
    duration: () => (Math.random() * 30 + 60) * 1000,
    dropCount: () => Math.ceil(Math.random() * 3),
    loop: true
});

pool.start$.subscribe(() => console.group(new Date()))
pool.drop$.subscribe(() => console.log(new Date()));
pool.stop$.subscribe(() => console.groupEnd())
pool.start();
```
```
0"                                               64"                                                     130"
|                                                ||                                                       ||
|[                   *                          ]||[        *     *                                      ]|| ...
|________________________________________________||_______________________________________________________||___
```
```
2019-05-07T15:47:54.602Z
  2019-05-07T15:48:21.821Z
2019-05-07T15:48:58.547Z
  2019-05-07T15:49:11.187Z
  2019-05-07T15:49:12.123Z
2019-05-07T15:50:04.570Z
  2019-05-07T15:50:30.671Z
  2019-05-07T15:50:37.575Z
```

### Play a short tone every 10 seconds, play a long tone near the beginning of each Pool
```typescript
const shortTone = new Howl('sounds/short.wav');
const pool = new AudioPool({
    assets: [{
        srcs: ['sounds/long.wav']
    }],
    duration: 10 * 1000,
    dropCount: 1,
    dropWeight: 1.8,
    loop: true
})

pool.start$.subscribe(() => shortTone.play())
pool.start();
```

### Play a cluster of clicks 3-5 times every 10 seconds
```typescript
const outerPool = new Pool({
    duration: 10 * 1000,
    dropCount: () => Math.ceil(Math.random() * 2 + 3),
    loop: true
})

outerPool.drop$.subscribe(() => {
    const innerPool = new AudioPool({
        assets: [{
                srcs: ['sounds/click-1.wav']
            }, {
                srcs: ['sounds/click-2.wav']
            }, {
                srcs: ['sounds/click-3.wav']
            }],
        dropCount: () => Math.ceil(Math.random() * 3 + 3),
        duration: 250
    })

    // the play$ observable provides a reference to the Howl
    // and the sound ID
    innerPool.play$.subscribe(howlRef => applyEffects(howlRef))
    innerPool.play();
})
outerPool.play();
```

### Play a short tone every 10 seconds, play a video clip between the tones, cycling through the sources in order
```typescript
const shortTone = new Howl('sounds/short.wav'),
    videos = [ 'vidOne', 'vidTwo', 'vidThree' ],
    formats = [ 'webm', 'mp4' ],
    containerElement = document.getElementById("video-container");
const pool = new VideoPool({
    assets: videos.map(fname => ({
        srcs: formats.map(format => `videos/${fname}.${format}`),
        formats: formats.map(format => `video/${format}`),
        ordered: true
    })),
    duration: 10 * 1000,
    dropCount: 1,
    loop: true
})

// VideoPool#preloadAll fires when all videos "canplaythrough"
// (also available on AudioPool)
pool.preloadAll()
    .subscribce(() => pool.play())

// VideoPool makes the element for you, but you have to
// add and remove it from the DOM yourself!
pool.play$.subscribe(video => {
    containerElement.appendChild(video.el)
    // The Video class wraps the element and provides a simple
    // event handler API.
    // VideoPool#destroyVideo removes the element from the DOM
    // and from its internal cache, avoiding detached nodes.
    video.once('end', () => pool.destroyVideo(video));

    // or to remove after the video has played 5 seconds:
    // video.after(5, () => pool.destroyVideo(video))
})
```

### Pause and play pools and the media they're playing
```typescript
const audioAssets = require('audioAssets')
const videoAssets = require('videoAssets')
const videoPool = new VideoPool({
    assets: videoAssets,
    duration: 60 * 1000,
    dropCount: 10,
    loop: true
});
const audioPool = new AudioPool({
    assets: audioAssets,
    duration: 60 * 1000,
    dropCount: 10,
    loop: true
});
const pools = [ audioPool, videoPool ]

videoPool.play$.subscribe(video => {
    containerElement.appendChild(video.el)
    video.once('end', () => video.el.remove())
})

let paused = false;
document.getElementById('pause-toggle')
    .addEventListener('click', e => {
        paused = !paused;
        pools.forEach(pool => paused ? pool.pause() : pool.resume())
        e.target.innerText = paused ? 'resume' : 'pause';
    })

pools.forEach(pool => pool.play())
```

### LICENSE

Copyright (C) 2019 Dave Riedstra. This program is distributed under the terms of the GNU General Public License.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
