/**
 * @module PoolTime
 */

import { Pool, IPoolArgs } from "./pool";
import { Video } from "./video";
import { Asset } from "./asset";
import { HowlReference } from "./howl-reference";
import { Observable, from, Subject } from "rxjs";
import { takeUntil, map, filter } from "rxjs/operators";

/**
 * Arguments for the [[MediaPool]] constructor.
 */
export interface IMediaPoolArgs extends IPoolArgs {
    assets?: Asset[];
    ordered?: boolean;
}

/**
 * # MediaPool
 * A [[Pool]] which handles time-based media. See [[VideoPool]] and [[AudioPool]].
 */
export abstract class MediaPool extends Pool {

    /**
     * List of media sources used by this Pool.
     */
    assets: Asset[];

    /**
     * Queued sources or media.
     */
    queue: any[];

    /**
     * Fires when media starts to play.
     */
    play$: Observable<HowlReference | Video>;

    /**
     * Determines whether the Assets will be played in order or at random.
     * (To weight the randomness, add duplicate Assets.)
     */
    ordered: boolean = false;

    /**
     * Currently playing media.
     */
    nowPlaying: any[] = [];

    protected _play$ = new Subject<HowlReference | Video>();

    /**
     * @param assets A list of [[Asset]]s handled by this Pool.
     * @param ordered Whether to consume the assets in the provided order
     * (default: `false`).
     */
    constructor({
        duration,
        dropCount,
        loop,
        dropDistributionWeight,
        getDropTime,
        assets,
        ordered
    } : IMediaPoolArgs) {
        super({
            duration,
            dropCount,
            loop,
            dropDistributionWeight,
            getDropTime,
        });

        this.assets = assets || [];
        this.ordered = !!ordered;

        this.play$ = from(this._play$.pipe(
            takeUntil(this._destroy$),
            filter(x => x !== undefined)
        ));

        this._drop$.pipe(takeUntil(this._destroy$))
            .subscribe(() => {
                this._play$.next(this.playNext())
            });

        this._start$.pipe(takeUntil(this._destroy$))
            .subscribe(() => this.stageAll())

        this._pause$.pipe(takeUntil(this._destroy$))
            .subscribe(() => this.pauseAll())

        this._resume$.pipe(takeUntil(this._destroy$))
            .subscribe(() => this.resumeAll())
    }

    stop() {
        super.stop()
        this.stopAll();
    }

    /**
     * Preloads all of the assets provided to the Pool.
     *
     * @returns     An Observable which fires when all the sources have
     * preloaded. The point at which the source is deemed to have been preloaded
     * is determined by the subclass.
     */
    abstract preloadAll(): Observable<void>;

    protected abstract playNext(): HowlReference | Video;
    protected abstract stageAll();
    protected abstract pauseAll();
    protected abstract resumeAll();
    protected abstract stopAll();

    /**
     * Gets the list of sources which will be played this queue
     */
    protected getAssetQueue(): Asset[] {
        const q: Asset[] = [];
        for (let i = 0; i < this.dropCount; i++) {
            let a: Asset;
            if (this.ordered)
                a = this.assets[i % this.assets.length]; // loop if fewer assets than drops
            else
                a = this.getRandomAsset();
            q.push(a);
        }
        return q;
    }

    /**
     * Returns a random source from our sources list
     */
    protected getRandomAsset(): Asset {
        const i = Math.floor(Math.random() * this.assets.length);
        return this.assets[i];
    }
}
