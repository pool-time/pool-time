/**
 * @module PoolTime
 *
 * Generates a weighted random number between min and max using gamma weighting.
 * https://stackoverflow.com/questions/445235/generating-random-results-by-weight-in-php/445363#445363
 * 
 * @param min 
 * @param max 
 * @param weight lower for higher numbers, higher for lower numbers, 1 for even distribution
 */
export function weightedRandom(min: number, max: number, weight: number): number {
    const r = Math.pow(Math.random(), weight);
    return r * (max - min) + min;
}
