/**
 * @module PoolTime
 */

import { Subject, Observable, from } from "rxjs";
import { weightedRandom } from "./weighted-random";

/**
 * Arguments for the [[Pool]] constructor.
 */
export interface IPoolArgs {
    duration: number | (() => number);
    dropCount: number | (() => number);
    loop?: boolean;
    dropDistributionWeight?: number | (() => number);
    getDropTime?: (duration: number, dropIndex: number) => number;
}

/**
 * # Pool
 * Represents a pool of time in which things (represented by drops) might
 * happen.
 */
export class Pool {
    //
    // Public API
    // 

    /**
     * Total duration of the current iteration of the pool (ms).
     */
    duration: number;

    /**
     * The number of times the present run will drop.
     */
    dropCount: number;

    /**
     * The distribution weighting for drops in the Pool * (1 = neutral, lower =
     * later, higher = earlier). Uses [[weightedRandom]] under the hood. Not
     * used if getDropTime provided.
     */
    dropDistributionWeight: number = 1;

    /**
     * Determines when in the Pool a drop will happen. If not defined,
     * dropDistributionWeight is used.
     */
    getDropTime: (poolDuration: number, dropIndex: number) => number;

    /**
     * Whether the Pool should restart when it ends.
     */
    loop: boolean = false;

    /**
     * Fires at the start of a Pool cycle.
     */
    start$: Observable<void>;

    /**
     * Fires at the end of a Pool cycle.
     */
    stop$: Observable<void>;

	/**
	 * Fires when the Pool stops and is not looping.
	 */
	destroy$: Observable<void>;

    /**
     * Fires for each drop in the Pool.
     * Callback is passed the drop index.
     */
    drop$: Observable<number>;

    /**
     * Fires when the Pool is paused.
     */
    pause$: Observable<void>;

    /**
     * Fires when the Pool is resumed.
     */
    resume$: Observable<void>;

    //
    // Private
    //

    // holder for each running timeout
    private timeouts: PoolTimeout[] = [];

    // subjects controlling the output observables
    protected _start$ = new Subject<void>();
    protected _stop$ = new Subject<void>();
    protected _destroy$ = new Subject<void>();
    protected _drop$ = new Subject<number>();
    protected _pause$ = new Subject<void>();
    protected _resume$ = new Subject<void>();

    // time remaining in the pool (ms)
    private remainingTime: number;

    // time when the currently running timeout started
    // nb: not when the Pool started
    private startTime: number;

    /**
     * Generates the duration of the current Pool
     */
    private getDuration: () => number;

    /**
     * Generates the number of Drops in this Pool
     */
    private getDropCount: () => number;

    /**
     * Generates the drop distribution weighting
     * (lower = later, higher = earlier, 1 = even)
     */
    private getDropDistributionWeight: () => number;

    /**
     * index of the next drop
     */
    private nextDropIndex = 0;

    /**
     * @param duration Duration of this pool (ms). If function, it will be
     * called each time the Pool starts to determine how long the currnt run
     * will last.
     * @param dropCount Number of times this Pool will drop. If function, it
     * will be called each time the Pool starts to determine how many times the
     * current run will drop.
     * @param loop Whether this Pool will loop.
     * @param dropDistributionWeight Distribution of drops within the pool:
     * 1 = neutral, lower = later, higher = earlier. Uses [[weightedRandom]]
     * under the hood. Not used if getDropTime is provided.
     * @param getDropTime Determines when in the Pool's duration a Drop will
     * happen. Parameter is the duration of the upcoming Pool. Return value will
     * be the Drop time in ms.
     */
    constructor({
        duration,
        dropCount,
        loop,
        dropDistributionWeight,
        getDropTime
    } : IPoolArgs) {
        this.start$ = from(this._start$);
        this.stop$ = from(this._stop$);
        this.destroy$ = from(this._destroy$);
        this.drop$ = from(this._drop$);
        this.pause$ = from(this._pause$);
        this.resume$ = from(this._resume$);

        if (typeof duration === 'number') {
            this.getDuration = () => duration;
        } else {
            this.getDuration = duration;
        }

        if (typeof dropCount === 'number') {
            this.getDropCount = () => dropCount;
        } else {
            this.getDropCount = dropCount;
        }

        if (loop !== undefined)
            this.loop = loop;

        if (dropDistributionWeight === undefined) {
            this.getDropDistributionWeight = () => 1;
        } else if (typeof dropDistributionWeight === 'number') {
            this.getDropDistributionWeight = () => dropDistributionWeight;
        } else {
            this.getDropDistributionWeight = dropDistributionWeight;
        }

        if (!!getDropTime)
            this.getDropTime = getDropTime;
    }

    //
    // Public methods
    //

    /**
     * Starts the pool.
     */
    start() {
        this.duration = this.remainingTime = this.getDuration();
        this.dropDistributionWeight = this.getDropDistributionWeight();
        this.dropCount = this.getDropCount();
        this.nextDropIndex = 0;
        this.createTimeouts();
        this.startAllTimeouts();
        this._onStart();
    }

    /**
     * Stops and resets the pool.
     */
    stop() {
        const _loop = this.loop;
        this.loop = false;
        this._onStop();
        this.loop = _loop; // preserve initial loop value
    }

    /**
     * Pauses the Pool.
     */
    pause() {
        this.clearAllTimeouts();
        this.updateRemainingTimes();
        this._onPause();
    }

    /**
     * Resumes the Pool.
     */
    resume() {
        this.startAllTimeouts();
        this._onResume();
    }

    // 
    // Private methods
    //

    // internal execution hooks

    private _onStart() {
        this._start$.next();
        if (!this.loop)
            this._start$.complete();
    }

    private _onDrop(dropIndex: number) {
        this._drop$.next(dropIndex);
        if (!this.loop && dropIndex == this.dropCount - 1)
            this._drop$.complete();
    }

    private _onStop() {
        this.deleteTimeouts();
        this._stop$.next();

        if (!this.loop) {
            this._start$.complete();
            this._drop$.complete();
            this._stop$.complete();
            this._pause$.complete();
            this._resume$.complete();
			this._destroy$.next();
			this._destroy$.complete();
        }

        if (this.loop)
            this.start();
    }

    private _onPause() {
        this._pause$.next();
    }

    private _onResume() {
        this._resume$.next();
    }


    /**
     * Callback supplied to pool timeout 
     */
    private poolTimeoutCb = () => this._onStop();

    /**
     * Callback supplied to drop timeout
     * Clears drop timeout and removes from 
     * this.timeouts, then calls onDrop() hook
     */
    private dropTimeoutCb = (t: PoolTimeout) => {
        clearTimeout(t.handle);

        const i = this.timeouts.indexOf(t);
        this.timeouts.splice(i, 1);

        this._onDrop(this.nextDropIndex);
        this.nextDropIndex++;
    }

    /**
     * Generates the time between the beginning
     * of the Pool and when a Drop drops.
     */
    private determineDropTime(i): number {
        if (!!this.getDropTime)
            return this.getDropTime(this.duration, i);

        return weightedRandom(0, this.duration, this.dropDistributionWeight);
    }

    /**
     * Populates timeouts array with parameters
     * for the Pool and Drop timing. Called at
     * the beginning of the Pool.
     */
    private createTimeouts() {
        // represents the pool itself
        this.timeouts.push({
            remainingTime: this.duration,
            callback: this.poolTimeoutCb
        })

        // represents each drop
        for (let i = 0; i < this.dropCount; i++) {
            this.timeouts.push({
                remainingTime: this.determineDropTime(i),
                callback: this.dropTimeoutCb
            })
        }
    }

    /**
     * starts the timeout; used in start() and resume()
     */
    private startAllTimeouts() {
        this.startTime = performance.now();
        this.timeouts.forEach(t => {
            t.handle = setTimeout(() => t.callback(t), t.remainingTime);
        })
    }

    /**
     * updates the remaining time for all timeouts
     */
    private updateRemainingTimes() {
        const now = performance.now();
        const elapsed = now - this.startTime;

        this.timeouts.forEach(t => {
            t.remainingTime = t.remainingTime - elapsed;
        })
    }

    /**
     * clears the timeouts
     */
    private clearAllTimeouts() {
        this.timeouts.forEach(t => {
            clearTimeout(t.handle);
            t.handle = undefined;
        })
    }

    /**
     * clears all timeouts, then deletes them
     */
    private deleteTimeouts() {
        this.clearAllTimeouts();
        this.timeouts = [];
    }

}

interface PoolTimeout {
    handle?: NodeJS.Timeout;
    remainingTime: number;
    callback: (PoolTimeout) => void;
}
