/**
 * @module PoolTime
 * # Asset
 * Represents a media asset which may be provided by one or more source files.
 */
export interface Asset {
    /**
     * List of source files which represent this Asset.
     */
    srcs: string[];

    /**
     * List of formats in the same order as their src counterparts. (Only used
     * with Video.)
     */
    formats?: string[];

    /**
     * Playback rate multiplier (1 is normal speed). If function, called each
     * time the Asset is played.
     */
    rate?: number | (() => number);

    /**
     * Starting seek point of the media (sec). If function, called each time the
     * Asset is played.
     */
    seek?: number | ((totalDuration: number) => number);
}
