/**
 * @module PoolTime
 */
import { Howl } from "howler";

/**
 * A reference to a sound played by a Howl.
 */
export interface HowlReference {
    /**
     * The Howl itself.
     */
    howl: Howl;

    /**
     * The sound's ID as returned from Howl.play.
     */
    id: number;
}
