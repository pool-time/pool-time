/**
 * @module PoolTime
 */

import { Asset } from "./asset";

/**
 * # Video
 * A wrapper around the `HTMLVideoElement` providing useful shorthands.
 */
export class Video {
    /**
     * The `HTMLVideoElement`'s currentSrc.
     */
    src: string;

    /**
     * The `HTMLVideoElement`.
     */
    el: HTMLVideoElement;

    /**
     * Whether the element's `canplaythrough` event has fired.
     */
    canplaythrough = false;

    /**
     * The playback rate (multiplier). See [[Asset.rate]].
     */
    rate?: number | (() => number);

    /**
     * The media's seek point (sec). See [[Asset.seek].
     */
    seek?: number | ((totalDuration) => number);

    private handlers: {event: string, callback: () => any}[] = [];

    /**
     * @param asset The [[Asset]] representing this video. The asset's sources
     * and formats are used in order.
     */
    constructor(asset: Asset) {
        this.el = this.makeElement(asset);
        this.src = this.el.currentSrc;
        this.once('canplaythrough', () => this.canplaythrough = true);
        this.rate = asset.rate;
        this.seek = asset.seek;
    }

    /**
     * Adds an event listener with `addEventListener`. Remove with [[off]].
     */
    on(event: string, cb: (...any) => void) {
        this.el.addEventListener(event, cb);
        this.handlers.push({event, callback: cb});
    }

    /**
     * Adds an event listener with [[on]] which is removed after the event
     * fires.
     */
    once(event: string, cb: (...any) => void) {
        const _el = this.el;
        const _cb = (...args: any[]) => {
            cb.apply(undefined, args);
            this.off(event, _cb);
        }
        this.el.addEventListener(event, _cb);
        this.handlers.push({event, callback: _cb});
    }

    /**
     * Removes an event listener added in [[on]] or [[once]].
     */
    off(event: string, callback: (...any) => any) {
        this.el.removeEventListener(event, callback);
        this.handlers = this.handlers.filter(h => h.callback != callback);
    }

    /**
     * Fire the callback once after playing for the duration (sec), or, if the
     * provided duration is negative, that many seconds from the video end.
     * Removes need for complicated timeout management.
     */
    after(duration: number, cb: (...any) => void) {
        if (duration < 0) {
            // if negative, count from end
            duration = this.el.duration + duration;
        } else {
            duration += this.el.currentTime; // account for seek
        }
        const _cb = (...args: any[]) => {
            if (this.el.currentTime >= duration) {
                cb.apply(undefined, args);
                this.off('timeupdate', _cb);
            }
        }
        this.on('timeupdate', _cb)
    }

    /**
     * Start the video playback
     */
    play() {
        this.el.play();
    }

    /**
     * Start the video playback after `canplaythrough` has fired.
     */
    playWhenReady() {
        if (this.canplaythrough) {
            this.play();
        } else {
            this.once('canplaythrough', () => this.play())
        }
    }

    /**
     * Pause the video playback.
     */
    pause() {
        this.el.pause();
    }

    /**
     * Pauses the video, removes all handlers, and removes element from DOM.
     */
    destroy() {
        this.pause();
        this.handlers.forEach(h => this.off(h.event, h.callback))
        this.el.remove();
    }

    private makeElement(asset: Asset): HTMLVideoElement {
        const v = document.createElement('video');
        v.preload = 'auto';
        v.muted = true;
        v.loop = true;

        asset.srcs.map((src, i) => ({src, format: asset.formats[i]}))
            .forEach(({src, format}) => {
                const s = document.createElement('source');
                s.src = src;
                if (format !== undefined)
                    s.setAttribute('type', format);
                v.appendChild(s);
            });

        return v;
    }
}

