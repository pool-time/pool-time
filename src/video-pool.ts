/**
 * @module PoolTime
 */

import { MediaPool } from './media-pool';
import { Video } from './video';
import { Asset } from './asset';
import { Observable, from, Subject } from "rxjs";
import { takeUntil, map, filter } from "rxjs/operators";

/**
 * # VideoPool
 * A [[MediaPool]] designed to handle playback of video files. Though this class
 * will handle the timing, loading, and element creation for you, you still need
 * to add the generated video element to the DOM. Do this by subscribing to
 * [[play$]].
 */
export class VideoPool extends MediaPool {
    /**
     * Fires when a sound begins to play. Use this instead of [[Pool.drop$]] to
     * gain access to the [[Video]].
     */
    play$: Observable<Video>;
    queue: Video[] = [];
    nowPlaying: Video[] = [];

    /**
     * Preloads all [[Asset]]s in the list. Returned observable fires when each
     * video's `canplaythrough` event fires.
     */
    preloadAll(): Observable<void> {
        const subj = new Subject<void>();
        let loaded = 0,
            len = this.assets.length;
        const countLoad = () => {
            loaded++;
            if (loaded == len) {
                subj.next();
                subj.complete();
            }
        };

        this.assets.forEach(s => {
            const v = VideoPool.getVideo(s);
            v.el.load()

            if (v.canplaythrough)
                countLoad();
            else
                v.once('canplaythrough', () => countLoad());
        });

        return from(subj);
    }

    /**
     * Removes Video from internal lists and calls [[Video.destroy]]. Use this
     * to remove the video from the DOM so that PoolTime can remove its
     * references to the DOM element.
     */
    destroyVideo(v: Video) {
        let i = this.nowPlaying.indexOf(v);
        while (i != -1) {
            this.nowPlaying.splice(i, 1)
            i = this.nowPlaying.indexOf(v);
        }
        v.destroy();
    }

    /**
     * Get a [[Video]] for a source.
     */
    static getVideo(asset: Asset): Video {
        return new Video(asset);
    }

    /**
     * Enqueues videos which will be played this cycle.
     * Determines which sources will be played, makes Videos for them, adds them
     * to the queue.
     */
    protected stageAll() {
        let assetQueue = this.getAssetQueue()

        assetQueue.forEach(asset => {
            let v = VideoPool.getVideo(asset);
            this.queue.push(v)
        });
    }

    /**
     * Plays the next video in the queue, removing it from the queue and adding
     * it to the nowPlaying list.
     */
    protected playNext(): Video {
        const video = this.queue.splice(0, 1)[0];
        const rate = typeof video.rate === "function" ? video.rate() : video.rate || 1;
        video.el.playbackRate = rate;

        const setSeek = () => {
            const seek = typeof video.seek === "function" ? video.seek(video.el.duration) : video.seek || 0;
            video.el.currentTime = seek;
            video.el.load();
            video.once('seeked', () => video.el.style.opacity = '');
        }

        if (video.el.duration === 0 || isNaN(video.el.duration)) {
            video.once('loadedmetadata', () => setSeek())
        } else {
            video.el.style.opacity = '0';
            setSeek();
        }

        video.once('play', () => this.nowPlaying.push(video))
        video.once('ended', () => this.destroyVideo(video))

        video.playWhenReady();
        return video;
    }

    /**
     * Pauses all currently playing videos
     */
    protected pauseAll() {
        this.nowPlaying.forEach(v => v.pause())
    }

    /**
     * Resumes all videos
     */
    protected resumeAll() {
        this.nowPlaying.forEach(v => v.play())
    }

    /**
     * Pauses all currently playing videos
     */
    protected stopAll() {
        this.nowPlaying.forEach(v => v.pause());
    }
}
