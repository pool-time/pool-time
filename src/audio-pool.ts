/**
 * @module PoolTime
 */

import { Pool } from "./pool";
import { MediaPool, IMediaPoolArgs } from "./media-pool";
import { HowlReference } from "./howl-reference";
import { Asset } from "./asset";
import { Howl } from "howler";
import { Observable, from, Subject } from "rxjs";
import { takeUntil, map, filter } from "rxjs/operators";

interface SoundReference {
    // The Asset representing the sound
    asset: Asset;
    // The sounds ID as returned by Howl.play
    id: number;
}

/**
 * Arguments for the [[AudioPool]] constructor.
 */
export interface IAudioPoolArgs extends IMediaPoolArgs {
    cleanEvery?: number
}

/**
 * # AudioPool
 * A [[MediaPool]] designed to handle playback of audio files. Uses Howler.js
 * under the hood.
 */
export class AudioPool extends MediaPool {
    /**
     * Fires when a sound begins to play. Use this instead of [[Pool.drop$]] to
     * gain access to the sound's [[HowlReference]].
     */
    play$: Observable<HowlReference>;
    queue: Asset[] = [];
    nowPlaying: SoundReference[] = [];

    /**
     * Cache of Howls across all instances of AudioPool. Optionally cleaned by
     * setting cleanEvery the [[constructor]] (recommended) or manually calling
     * [[AudioPool.cleanCache]]().
     */
    static HOWL_CACHE: Map<Asset, Howl> = new Map();

    /**
     * List of sources that are enqueued for all extant AudioPools. Used to
     * help cache maintenance.
     */
    static QUEUE: Map<Asset, number> = new Map();

    /**
     * @param cleanEvery The number of iterations before
     * [[AudioPool.cleanCache]] is automatically called.
     */
    constructor({
        duration,
        dropCount,
        loop,
        dropDistributionWeight,
        getDropTime,
        assets,
        ordered,
        cleanEvery
    } : IAudioPoolArgs) {
        super({
            duration,
            dropCount,
            loop,
            dropDistributionWeight,
            getDropTime,
            assets,
            ordered
        });
        let loopCounter = 0;

        this._start$.pipe(takeUntil(this._destroy$))
            .subscribe(() => { loopCounter = loopCounter % cleanEvery + 1; })

        if (cleanEvery > 0) {
            this._stop$.pipe(
                filter(() => loopCounter % cleanEvery === 0),
                takeUntil(this._destroy$)
            ).subscribe(() => AudioPool.cleanCache());
        }
    }

    /**
     * Cleans the entire [[AudioPool.HOWL_CACHE]] by unloading Howls which
     * aren't playing, aren't queued, and aren't loading. Automatically
     * called if `cleanEvery` is set in [[AudioPool.constructor]].
     */
    static cleanCache() {
        AudioPool.HOWL_CACHE.forEach((howl, src) => {
            const isInQueue = AudioPool.QUEUE.has(src);
            const isPlaying = howl.playing();
            const isLoading = howl.state() === "loading";

            if (!isInQueue && !isPlaying && !isLoading) {
                AudioPool.HOWL_CACHE.delete(src);
                howl.unload();
            }
        })
    }

    /**
     * Preloads all [[Asset]]s in the list. Returned observable fires when each
     * sound's Howl fires its `load` event.
     */
    preloadAll(): Observable<void> {
        const subj = new Subject<void>();
        let loaded = 0,
            len = this.assets.length;
        const countLoad = () => {
            loaded++;
            if (loaded == len) {
                subj.next();
                subj.complete();
            }
        };

        this.assets.forEach(s => AudioPool.getHowl(s, () => countLoad()));
        
        return from(subj)
    }

    /**
     * Gets or makes a Howl for a src sound as needed.  Use this to interface
     * with the cache or to make new Howls.
     *
     * If a Howl doesn't exist for a source, getHowl makes one, adds it to the
     * cache, and returns it.
     *
     * If a Howl does already exist, getHowl simply returns it.
     */
    static getHowl(asset: Asset, onload?: () => any): Howl {
        if (AudioPool.HOWL_CACHE.has(asset)) {
            if (!!onload)
                onload();
            return AudioPool.HOWL_CACHE.get(asset);
        } else {
            let h = new Howl({
                src: asset.srcs,
                preload: true,
                onload: onload
            });
            AudioPool.HOWL_CACHE.set(asset, h);
            return h;
        }
    }

    /**
     * Selects sources from our list, loads them,
     * and adds them to the queue when they're loaded
     */
    protected stageAll() {
        let assetQueue = this.getAssetQueue();
        assetQueue.forEach(asset => {
            const h = AudioPool.getHowl(asset, () => this.addToQueue(asset));

            // remove track from nowplaying when done
            h.once('end', id => {
                const npSRef = this.nowPlaying.find(s => s.id == id);
                if (npSRef !== undefined)
                    this.nowPlaying.splice(this.nowPlaying.indexOf(npSRef), 1);
            });
        });
    }

    private addToQueue(src) {
        this.queue.push(src);
        let oldVal = AudioPool.QUEUE.get(src) || 0;
        AudioPool.QUEUE.set(src, oldVal + 1)
    }

    private removeFromQueue(src) {
        this.queue.splice(this.queue.indexOf(src), 1);
        const oldVal = AudioPool.QUEUE.get(src);
        AudioPool.QUEUE.set(src, oldVal - 1);
        if (oldVal === 1)
            AudioPool.QUEUE.delete(src);
    }

    /**
     * Plays the next sound in the queue and
     * adds it to the nowPlaying list
     */
    protected playNext(): HowlReference {
        // take the first loaded sound
        const q = this.queue.filter(s => AudioPool.getHowl(s).state() === "loaded")

        if (q.length == 0) return;

        const asset = q[0];
        const howl = AudioPool.getHowl(asset)

        const rate = typeof asset.rate === "function" ? asset.rate() : asset.rate || 1;
        howl.rate(rate)

        const seek = typeof asset.seek === "function" ? asset.seek(howl.duration()) : asset.seek || 0;

        const id = howl.play();
        howl.seek(seek, id)

        this.removeFromQueue(asset);
        this.nowPlaying.push({
            asset: asset,
            id: id
        });

        return {id, howl};
    }

    /**
     * Pauses all currently playing sounds
     */
    protected pauseAll() {
        this.nowPlaying.map(s => ({
            h: AudioPool.getHowl(s.asset),
            id: s.id
        })).forEach(s => s.h.pause(s.id));
    }

    /**
     * Resumes all previously playing sounds
     */
    protected resumeAll() {
        this.nowPlaying.map(s => ({
            h: AudioPool.getHowl(s.asset),
            id: s.id
        })).forEach(s => s.h.play(s.id))
    }

    /**
     * Pauses all currently playing sounds
     */
    protected stopAll() {
        this.nowPlaying.map(s => ({
            h: AudioPool.getHowl(s.asset),
            id: s.id
        })).forEach(s => s.h.stop(s.id));
    }
}
